#!/usr/bin/env python3


import os
import sys
import time
import subprocess
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.options import Options

found_pages = []
homedir = os.getenv("HOME")

def get_single_score_page(webdrv, page):
    should_continue = True

    try:
        result = wait.until(presence_of_element_located((By.CSS_SELECTOR, "img#score_" + str(page))))
        found_pages.append(result.get_attribute("src"))
        webdrv.execute_script("arguments[0].scrollIntoView();", result)

    except TimeoutException as e:
        print("No more pages found")
        should_continue = False

    return should_continue

def get_score_pages(webdrv):
    page = 0
    should_continue = True

    while should_continue:
        should_continue = get_single_score_page(webdrv, page)
        page += 1

def get_score_title(webdrv):
    title = webdrv.find_element_by_tag_name("h1")
    return title

def setup_options():
    options = Options();
    options.headless = True
    options.set_preference("browser.download.folderList",1);
    return options

def download(link):
    page_name = link.split("/")[-1].split("?")[0]
    download_path = "{}/Downloads/{}".format(homedir, page_name)
    subprocess.run(["wget", link, "-O", download_path])
    return download_path

def svg_to_png(filename):
    command = "inkscape -z -e " + filename + ".png" + " -w 794 -h 1123 " + filename
    command = command.split()
    subprocess.run(command)
    return filename + ".png"

def add_white_background(filename):
    command = "convert -flatten {} {}".format(filename, filename)
    command = command.split()
    subprocess.run(command)

def compile_to_pdf(filenames, title):
    command = "convert "
    for filename in filenames:
        command += "{} ".format(filename)

    command = command.split()
    command .append("{}.pdf".format(title))
    subprocess.run(command)

def is_png_format(filename):
    return ".png" in filename

def delete_downloaded_files(filenames):
    for filename in filenames:
        os.remove(filename)

if __name__ == "__main__":
    url = input("Musescore sheet link > ")
    try:
        driver = webdriver.Firefox(options = setup_options())
    except WebDriverException:
        print()
        print("ERROR: geckodriver not found, ensure it is in $PATH")
        print()
        sys.exit()

    wait = WebDriverWait(driver, 4)
    driver.get(url)
    get_score_pages(driver)
    title = get_score_title(driver).text
    driver.quit()

    filenames = []

    for page in found_pages:
        downloaded_file = download(page)
        if not is_png_format(downloaded_file):
            downloaded_file = svg_to_png(downloaded_file)

        add_white_background(downloaded_file)
        filenames.append(downloaded_file)

        time.sleep(1)

    compile_to_pdf(filenames, title)
    delete_downloaded_files(filenames)

